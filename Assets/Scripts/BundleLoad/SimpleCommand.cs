using System.Threading.Tasks;
using UnityEngine;

namespace BundleLoad
{
    public class SimpleCommand : ICommand
    {
        public async Task<ICommand> Play()
        {
            await Task.Delay(Random.Range(0, 5));
            return this;
        }
    }
}
