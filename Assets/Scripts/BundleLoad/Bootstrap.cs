using UnityEngine;

namespace BundleLoad
{
    public class Bootstrap : MonoBehaviour, IBootstrap
    {
        private IGameManager gameManager;
        private ICommandManager commandManager;
        private IBundleManager bundleManager;

        public void Init(IBundleManager bundleManager)
        {
            this.bundleManager = bundleManager;

            CreateCommandManager();
            CreateGameManager();
        }

        public void CreateCommandManager()
        {
            commandManager = new CommandManager();
        }

        public void CreateGameManager()
        {
            gameManager = new GameManager(commandManager, bundleManager);
            gameManager.GameFinished += OnGameFinished;
        }

        private void OnGameFinished()
        {
            bundleManager.UnloadBundle();
        }
    }
}
