using System;

namespace BundleLoad
{
    public class CommandManager : ICommandManager
    {
        public event Action<ICommand> CommandFinished;
        public event Action QueueFinished;

        private ICommand[] commands;

        public void Init(INode node)
        {
        }

        public void AddCommands(ICommand[] commands)
        {
            this.commands = commands;
        }

        public async void Execute()
        {
            for (int i = 0; i < commands.Length; i++)
            {
                await commands[i].Play();
                CommandFinished?.Invoke(commands[i]);
            }

            QueueFinished?.Invoke();
        }
    }
}
