using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace BundleLoad
{
    public class BundleManager : MonoBehaviour, IBundleManager
    {
        public AssetBundle CurrentBundle { get; private set; }

        //just for testing purposes
        private void Start()
        {
            string url = Path.Combine(Application.streamingAssetsPath, "Serie1/V1/Android/Serie1V1_android.bundle");
            UnityWebRequest bundleRequest = UnityWebRequestAssetBundle.GetAssetBundle(url);
            bundleRequest.SendWebRequest().completed += (p) =>
            {
                if (bundleRequest.result != UnityWebRequest.Result.Success)
                    return;

                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(bundleRequest);
                LoadBundle(bundle);
            };
        }

        public void LoadBundle(AssetBundle bundle)
        {
            CurrentBundle = bundle;

            LoadAssets();
        }

        public void UnloadBundle()
        {
        }

        private void LoadAssets()
        {
            string[] scenePaths = CurrentBundle.GetAllScenePaths();
            for (int i = 0; i < scenePaths.Length; i++)
            {
                if (!scenePaths[i].ToLower().Contains("bootstrap"))
                    continue;
                SceneManager.LoadSceneAsync(scenePaths[i], LoadSceneMode.Additive).completed += BootstrapReady;
                break;
            }
        }

        private void BootstrapReady(AsyncOperation ao)
        {
            IBootstrap bootstrap = FindObjectOfType<Bootstrap>();
            bootstrap.Init(this);
        }
    }
}
