using System;

namespace BundleLoad
{
    public class GameManager : IGameManager//, INetClient
    {
        public event Action GameFinished;

        private ICommandManager commandManager;
        private IBundleManager bundleManager;
        private INode[] nodes;

        public GameManager(ICommandManager commandManager, IBundleManager bundleManager)
        {
            this.commandManager = commandManager;
            this.bundleManager = bundleManager;

            //Here we suppose to somehow connect to server
        }

        public void SetConnector(/*IServerConnector connector*/)
        {

        }

        public void StartGame(INode[] nodes)
        {
            this.nodes = nodes;
        }

        public void StopGame()
        {
        }

        public void PlayNode()
        {
        }
    }
}
