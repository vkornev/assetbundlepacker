using System;
using System.Threading.Tasks;
using UnityEngine;

namespace BundleLoad
{
    public interface IBundleManager
    {
        AssetBundle CurrentBundle { get; }

        void LoadBundle(AssetBundle bundle);
        void UnloadBundle();
    }

    public interface IBootstrap
    {
        void Init(IBundleManager bundleManager);
        void CreateGameManager();
        void CreateCommandManager();
    }

    public interface IGameManager
    {
        event Action GameFinished;

        void StartGame(INode[] nodes);
        void PlayNode();
        void StopGame();
    }

    public interface ICommandManager
    {
        event Action<ICommand> CommandFinished;
        event Action QueueFinished;

        void Init(INode node);
        void AddCommands(ICommand[] commands);
        void Execute();
    }

    public interface INode
    {
        void Play();
    }

    public interface ICommand
    {
        Task<ICommand> Play();
    }
}
