using UnityEngine;
using UnityEditor;
using System.IO;

public class AssetBundleManager : EditorWindow
{
    private const string k_contentFileName = "NewSerieContent";

    [SerializeField] private SerieContentBundle settings;
    [SerializeField] private ContentPath contentPath;

    [MenuItem("Window/AssetBundle Manager...")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<AssetBundleManager>("Asset Bundle Manager");
    }

    private void OnDestroy()
    {
        SaveBundle();
    }

    private void OnGUI()
    {
        //showing our object where we suppose to save the bundle setup
        ScriptableObject scriptableObj = this;
        SerializedObject serialObj = new SerializedObject(scriptableObj);
        SerializedProperty settingsProp = serialObj.FindProperty("settings");
        EditorGUILayout.PropertyField(settingsProp, true);
        SerializedProperty contentPathProp = serialObj.FindProperty("contentPath");
        EditorGUILayout.PropertyField(contentPathProp, true);
        serialObj.ApplyModifiedProperties();

        // if this save object is null - show the interface to create it
        // or just use the whole thing without saving it
        if (settings == null)
        {
            //create a save object
            if (GUILayout.Button("Create settings"))
                CreateSettingsFiles();
        }
        else
            ShowContent();

        if (GUILayout.Button("Pack"))
        {
            if (settings != null)
                BuildAllAssetBundles(settings, ref contentPath);
        }
    }

    private void ShowContent()
    {
        SerializedObject serialSettings = new SerializedObject(settings);
        SerializedProperty bundleNameProp = serialSettings.FindProperty("bundleName");
        EditorGUILayout.PropertyField(bundleNameProp, true);
        SerializedProperty bundleVariantProp = serialSettings.FindProperty("bundleVariant");
        EditorGUILayout.PropertyField(bundleVariantProp, true);
        SerializedProperty itemsProp = serialSettings.FindProperty("items");
        EditorGUILayout.PropertyField(itemsProp, true);
        EditorGUI.BeginDisabledGroup(true);
        SerializedProperty versionProp = serialSettings.FindProperty("unityVersion");
        EditorGUILayout.PropertyField(versionProp, true);
        SerializedProperty timeProp = serialSettings.FindProperty("modificationTime");
        EditorGUILayout.PropertyField(timeProp, true);
        EditorGUI.EndDisabledGroup();
        serialSettings.ApplyModifiedProperties();
    }

    private void CreateSettingsFiles()
    {
        SerieContentBundle asset = ScriptableObject.CreateInstance<SerieContentBundle>();
        AssetDatabase.CreateAsset(asset, $"Assets/{k_contentFileName}.asset");
        AssetDatabase.SaveAssets();
        ContentPath assetContentPath = CreateContentPath(k_contentFileName);

        settings = asset;
        contentPath = assetContentPath;

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    private static ContentPath CreateContentPath(string contentFileName)
    {
        ContentPath assetContentPath = ScriptableObject.CreateInstance<ContentPath>();
        AssetDatabase.CreateAsset(assetContentPath, $"Assets/{contentFileName}_contentPath.asset");
        AssetDatabase.SaveAssets();
        return assetContentPath;
    }

    private void SaveBundle()
    {
        if (settings)
            settings.UpdateBundle();
    }

    private static void BuildAllAssetBundles(SerieContentBundle bundleSettings, ref ContentPath contentPath)
    {
        if (bundleSettings.Items == null || bundleSettings.Items.Length == 0)
            return;

        AssetBundleBuild[] builds = new AssetBundleBuild[1];
        builds[0].assetNames = new string[bundleSettings.Items.Length];
        for (int i = 0; i < bundleSettings.Items.Length; i++)
            builds[0].assetNames[i] = AssetDatabase.GetAssetPath(bundleSettings.Items[i]);

        builds[0].assetBundleName = bundleSettings.BundleName;
        builds[0].assetBundleVariant = bundleSettings.BundleVariant;

        if (contentPath == null)
            contentPath = CreateContentPath(k_contentFileName);

        //we don't need the bundles to go right back into project, right?
        if (contentPath.path == null || contentPath.path.Length == 0)
            contentPath.path = $"../AssetBundles/{bundleSettings.BundleName}/{bundleSettings.BundleVariant}";

        string androidPath = Path.Combine(Application.dataPath, contentPath.path, "Android/");
        string androidFolder = EnsurePath(androidPath);
        Debug.Log($"Building Android into [{androidFolder}]");
        BuildPipeline.BuildAssetBundles(androidFolder, builds, BuildAssetBundleOptions.None, BuildTarget.Android);

        string bundleFilePath = $"{androidFolder}/{bundleSettings.BundleName}.{bundleSettings.BundleVariant}";
        if (File.Exists(bundleFilePath))
        {
            string dest = $"{androidFolder}/{bundleSettings.BundleName}{bundleSettings.BundleVariant}_android.bundle";
            File.Move(bundleFilePath, dest);
        }
    }

    /// <summary>
    /// Make sure the path exist
    /// </summary>
    private static string EnsurePath(string path)
    {
        string folder = null;
        try
        {
            folder = Path.GetFullPath(path);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
        }
        catch (IOException ioex)
        {
            Debug.LogError(ioex.Message);
        }
        return folder;
    }
}
