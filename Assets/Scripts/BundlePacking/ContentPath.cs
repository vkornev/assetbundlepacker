using UnityEngine;

public class ContentPath : ScriptableObject
{
    [SerializeField] public string path;
}
