using UnityEngine;
using UnityEditor;

[System.Serializable]
public class SerieContentBundle : ScriptableObject
{
    public string BundleName => bundleName;
    public string BundleVariant => bundleVariant;
    public Object[] Items => items;

    [SerializeField] private string bundleName = string.Empty;
    [SerializeField] private string bundleVariant = string.Empty;
    [SerializeField] private Object[] items;
    [SerializeField] private string unityVersion = string.Empty;
    [SerializeField] private string modificationTime;

    public void Init(string bundleName, string bundleVariant, Object[] items)
    {
        this.bundleName = bundleName;
        this.bundleVariant = bundleVariant;
        this.items = items;
        UpdateBundle();
    }

    public void UpdateBundle()
    {
        if (EditorUtility.IsDirty(this.GetInstanceID()))
        {
            this.unityVersion = Application.unityVersion;
            this.modificationTime = System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm");
        }
    }

    private void OnValidate()
    {
        UpdateBundle();
    }
}
